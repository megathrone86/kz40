﻿using kz40.Data;
using kz40.DTO.Auth;
using kz40.Exceptions;
using kz40.Extensions;
using kz40.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace kz40.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : AppControllerBase
    {
        KeysProvider _keysProvider;

        public AuthController(DataContext dbContext, IConfiguration configuration, KeysProvider keysProvider)
                : base(dbContext, configuration)
        {
            _keysProvider = keysProvider;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody]LoginRequest request)
        {
            if (request.Email.Length > 200)
                return BadRequest();
            var email = request.Email.ToLower().Trim();

            try
            {
                var reCaptchaSecretKey = Configuration.GetValue<string>("ReCaptchaSecretKey");
                await GoogleReCaptcha.Verify(request.CaptchaResponse, reCaptchaSecretKey);
            }
            catch (WrongCaptchaException ex)
            {
                return BadRequest(new LoginResponse
                {
                    Error = $"Ошибка проверки капчи."
                });
            }

            try
            {
                var users = (await DbContext.Users.Where(t => !t.IsDeleted && t.Email == email).ToArrayAsync());
                var user = users.FirstOrDefault();

                var token = user.CreateAuthToken(_keysProvider.GetTokenEncryptKey(), _keysProvider.GetTokenSignKey(), request.Password);

                return Ok(new LoginResponse() { Token = token });
            }
            catch (LoginException ex)
            {
                return BadRequest(new LoginResponse() { Error = "Ошибка входа" });
            }
            catch (Exception ex)
            {
                return BadRequest(new LoginResponse()
                {
#if SHOW_ERRORS
                    Error = ex.ToString()
#endif
                });
            }
        }

        [HttpGet]
        [Authorize]
        [Route("login")]
        public async Task<ActionResult> GetAuthorizedUser()
        {
            if (!CurrentUserId.HasValue)
                return BadRequest();

            await FillCurrentUser();

            return Ok(new
            {
                id = CurrentUser.Id,
                email = CurrentUser.Email,
            });
        }
    }
}
