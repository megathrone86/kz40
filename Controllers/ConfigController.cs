﻿using kz40.Data;
using kz40.DTO.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace kz40.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ConfigController : AppControllerBase
    {
        readonly string[] AllowedParameters = new string[]
        {
            "ReCaptchaSiteKey"
        };

        public ConfigController(DataContext dbContext, IConfiguration configuration)
            : base(dbContext, configuration)
        {
        }

        [HttpGet]
        [Route("{name}")]
        public ActionResult<GetParamResponse> GetParam(string name)
        {
            if (!AllowedParameters.Contains(name))
                return Forbid();

            var value = Configuration.GetValue<string>(name);
            return Ok(new GetParamResponse() { Value = value });
        }
    }
}
