﻿using kz40.Data;
using kz40.Data.Models;
using kz40.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace kz40.Controllers
{
    public class AppControllerBase : ControllerBase
    {
        private Guid? _userId;

        public Guid? CurrentUserId
        {
            get
            {
                if (!_userId.HasValue)
                {
                    var value = HttpContext.User.GetClaimValue(Consts.Claim_UserId);
                    _userId = string.IsNullOrWhiteSpace(value) ? (Guid?)null : Guid.Parse(value);
                }
                return _userId;
            }
        }

        //public UserRole? UserRole
        //{
        //    get
        //    {
        //        if (!_role.HasValue)
        //        {
        //            _role = HttpContext.User.GetClaimValue(Consts.Claim_RoleId).CastToEnum<UserRole>();
        //        }
        //        return _role;
        //    }
        //}

        protected DataContext DbContext { get; }
        protected IConfiguration Configuration { get; }

        protected User CurrentUser { get; private set; }

        public AppControllerBase(DataContext dbContext, IConfiguration configuration)
        {
            DbContext = dbContext;
            Configuration = configuration;
        }

        protected async Task FillCurrentUser()
        {
            if (CurrentUser == null)
            {
                CurrentUser = await DbContext.Users.FirstAsync(t => t.Id == CurrentUserId.Value && !t.IsDeleted);
            }
        }
    }
}