﻿using kz40.Data;
using kz40.Extensions;
using kz40.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace kz40.Infrastructure
{
    /// <summary>
    /// Данный фильтр проверяет валидность токена для методов, в которых присутствует атрибут Authorize
    /// </summary>
    public class CheckUserFilter : IAsyncActionFilter
    {
        readonly DataContext _dbContext;
        readonly KeysProvider _keysProvider;

        public CheckUserFilter(DataContext dbContext, KeysProvider keysProvider)
        {
            _dbContext = dbContext;
            _keysProvider = keysProvider;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerActionDescriptor != null)
            {
                var actionAttributes = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true);
                if (actionAttributes.Any(t => t is AuthorizeAttribute))
                {
                    await CheckAuthorizedUser(context);
                }
            }

            await next.Invoke();
        }

        async Task CheckAuthorizedUser(ActionExecutingContext context)
        {
            var userIdString = context.HttpContext.User.GetClaimValue(Consts.Claim_UserId);
            if (!Guid.TryParse(userIdString, out Guid userId))
                throw new SecurityException();

            var accessToken = context.HttpContext.User.GetClaimValue(Consts.Claim_AccessToken);

            var user = await _dbContext.Users.FirstAsync(t => t.Id == userId && !t.IsDeleted);
            if (user.GetAcessToken() != accessToken)
                throw new SecurityException();
        }
    }
}