﻿using System.Linq;
using System.Security.Claims;

namespace kz40.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetClaimValue(this ClaimsPrincipal principal, string claimName)
        {
            var claim = principal.Claims.FirstOrDefault(x => x.Type == claimName);

            if (claim == null)
                return null;

            return claim.Value;
        }
    }
}
