﻿using kz40.Data.Models;
using kz40.Exceptions;
using kz40.Services;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace kz40.Extensions
{
    public static class UserExtensions
    {
        /// <summary>
        /// Присвоить новый пароль
        /// </summary>
        public static void SetPassword(this User user, string password)
        {
            user.HashPassword = HashService.GenHash(password, user.PassKey);
        }

        /// <summary>
        /// Сгенерировать токен доступа
        /// </summary>
        public static string CreateAcessToken(this User user)
        {
            return user.GetAcessToken();
        }

        /// <summary>
        /// Получить текущий токен доступа
        /// </summary>
        public static string GetAcessToken(this User user) => user.HashPassword;

        /// <summary>
        /// Сгенерировать токен доступа и (опционально) перед этим проверить пароль
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string CreateAuthToken(this User user, byte[] signatureKey, byte[] encryptionKey, string password = null)
        {
            if (password != null)
            {
                var hashPassword = HashService.GenHash(password, user.PassKey);
                if (hashPassword != user.HashPassword)
                    throw new LoginException();
            }

            var userId = user.Id.ToString("N");
            var accessToken = user.CreateAcessToken();
            //var roleId = (int)user.Role;
            var roleId = 0;

            var token = GenerateJwtToken(userId, accessToken, roleId);
            return token;

            string GenerateJwtToken(string userId, string accessToken, int roleId)
            {
                var claims = new Claim[]
                {
                    new Claim(Consts.Claim_UserId, userId),
                    new Claim(Consts.Claim_AccessToken, accessToken),
                    new Claim(Consts.Claim_RoleId, roleId.ToString())
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var currentDate = DateTime.UtcNow;

                var token = tokenHandler.CreateJwtSecurityToken(
                    issuer: Consts.Token_Issuer,
                    audience: Consts.Token_Audience,
                    subject: new ClaimsIdentity(claims),
                    issuedAt: currentDate,
                    notBefore: currentDate,
                    expires: DateTime.UtcNow.AddMinutes(Consts.Token_LifeTimeMinutes),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(signatureKey), SecurityAlgorithms.HmacSha256),
                    encryptingCredentials: new EncryptingCredentials(new SymmetricSecurityKey(encryptionKey),
                        JwtConstants.DirectKeyUseAlg, SecurityAlgorithms.Aes256CbcHmacSha512)
                );

                return tokenHandler.WriteToken(token);
            }
        }
    }
}
