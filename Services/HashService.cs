﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace kz40.Services
{
    public static class HashService
    {
        public static string GenHash(string value, string salt)
        {
            var inputBytes = Encoding.UTF8.GetBytes(value + salt);
            var hashedBytes = SHA256.Create().ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes);
        }
    }
}