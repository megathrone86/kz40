﻿using kz40.Data;
using kz40.Data.Models;
using System;
using System.Linq;

namespace kz40.Services
{
    public class DatabaseSeeder
    {
        DataContext _context;

        public DatabaseSeeder(DataContext context)
        {
            this._context = context;
        }

        public void InitDatabase()
        {
            if (!_context.Users.Any(t => t.Name == "admin"))
            {
                AddUser(_context, new User()
                {
                    Name = Consts.DefaultAdminName,
                    Email = Consts.DefaultAdminEmail,
                    PassKey = Guid.NewGuid().ToString().Substring(0, 8),
                    //Role = UserRole.Admin,
                }, Consts.DefaultAdminPassword);
            }

            _context.SaveChanges();
        }

        void AddUser(DataContext context, User user, string password)
        {
            user.HashPassword = HashService.GenHash(password, user.PassKey);
            context.Users.Add(user);
        }
    }
}
