﻿using System;
using System.IO;

namespace kz40.Services
{
    public class KeysProvider
    {
        Key _tokenSignKey = new Key(Consts.TokenSignKeyName, () => KeyGenerator.GetKey(512));
        Key _tokenEncryptKey = new Key(Consts.TokenEncryptKeyName, () => KeyGenerator.GetKey(512));

        public KeysProvider()
        {
        }

        internal void Init()
        {
        }

        public byte[] GetTokenSignKey() => _tokenSignKey.Get();
        public byte[] GetTokenEncryptKey() => _tokenEncryptKey.Get();

        class Key
        {
            string _fileName;
            Func<byte[]> _createFunc;

            byte[] _value;

            public Key(string fileName, Func<byte[]> createFunc)
            {
                _fileName = fileName;
                _createFunc = createFunc;
            }

            public byte[] Get()
            {
                if (_value == null)
                {
                    if (File.Exists(_fileName))
                    {
                        _value = File.ReadAllBytes(_fileName);
                    }
                    else
                    {
                        _value = _createFunc.Invoke();
                        File.WriteAllBytes(_fileName, _value);
                    }
                }

                return _value;
            }
        }
    }
}
