﻿using kz40.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace kz40.Services
{
    public class DatabaseChecker
    {
        IApplicationBuilder _app;

        public DatabaseChecker(IApplicationBuilder app)
        {
            _app = app;
        }

        public async Task CheckAsync()
        {
            using var serviceScope = _app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();

            try
            {
                await CheckFiles(context);
            }
            catch (Exception ex)
            {
                Debugger.Break();
                Debug.WriteLine(ex);
            }
        }

        async Task CheckFiles(DataContext context)
        {
            //do
            //{
            //    var files = context.Files.Where(t => t.CreateDateSearchString == null).Take(10);
            //    if (!files.Any())
            //        break;
            //    files.ForEach(f => f.UpdateCreateDateSearchString());
            //    await context.SaveChangesAsync();

            //} while (true);
        }
    }
}
