﻿using kz40.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace kz40.Services
{
    public class GoogleReCaptcha
    {
        const string Url = "https://www.google.com/recaptcha/api/siteverify";

        public static async Task Verify(string recaptchaResponse, string secretKey)
        {
            if (string.IsNullOrWhiteSpace(recaptchaResponse))
                throw new WrongCaptchaException();

            var values = new Dictionary<string, object>
            {
                { "secret", secretKey },
                { "response", recaptchaResponse }
            };

            var response = await HttpHelper.PostAsync(Url, values);
            var responseContent = await response.Content.ReadAsStringAsync();
            var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleCaptchaResponse>(responseContent);
            if (parsedResponse.success != true)
                throw new WrongCaptchaException();
        }

        class GoogleCaptchaResponse
        {
            public bool? success { get; set; }
            public string challenge_ts { get; set; }
            public string hostname { get; set; }
        }
    }
}
