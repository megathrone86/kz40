﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace kz40.Services
{
    public class HttpHelper
    {
        static readonly HttpClient client = new HttpClient();

        public static Task<HttpResponseMessage> PostAsync(string url, Dictionary<string, object> values)
        {
            var content = new FormUrlEncodedContent(values.ToDictionary(t => t.Key, t => t.Value.ToString()));
            return client.PostAsync(url, content);
        }
    }
}
