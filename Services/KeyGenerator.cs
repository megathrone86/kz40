﻿using System;

namespace kz40.Services
{
    public class KeyGenerator
    {
        static Random _random = new Random();

        public static byte[] GetKey(int size)
        {
            var result = new byte[size];
            _random.NextBytes(result);
            return result;
        }
    }
}
