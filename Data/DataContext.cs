﻿using kz40.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Toolbelt.ComponentModel.DataAnnotations;

namespace kz40.Data
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        string _schema = null;

        public DataContext() : base()
        {
        }

        public DataContext(IConfiguration configuration, DbContextOptions options) : base(options)
        {
            _schema = configuration.GetConnectionString("CustomSchema");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            if (!string.IsNullOrEmpty(_schema))
            {
                //это нужно для того, чтобы подключаться к БД с боевого сервера (там другое имя схемы)
                modelBuilder.HasDefaultSchema(_schema);
            }

            modelBuilder.BuildIndexesFromAnnotations();
        }
    }
}
