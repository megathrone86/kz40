﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kz40.Data.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        [MaxLength(100)]
        public string LastName { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Password { get; set; }
        [MaxLength(100)]
        public string PassKey { get; set; }
        [MaxLength(500)]
        public string HashPassword { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime CreateDate { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
    }
}
