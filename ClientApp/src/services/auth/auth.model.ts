import { NgFormSelectorWarning } from "@angular/forms";

export interface LoginResponse {
    token: string;
    error: string;
    errorNoCaptcha: boolean;
}

export interface User {
    id: string;
    email: string;
}