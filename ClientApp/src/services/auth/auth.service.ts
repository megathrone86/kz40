import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { LoginResponse, User } from './auth.model';
import { getAuthHeaders, setAuthToken, clearAuthToken, getAuthToken } from 'src/helpers/auth-helper';

@Injectable()
export class AuthService {
	currentUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);

	constructor(
		private http: HttpClient,
	) {
		this.loadUser().subscribe();
	}

	login(email: string, password: string, captchaResponse: string) {
		return this.http.post<LoginResponse>('api/auth/login', { email, password, captchaResponse })
			.pipe(
				catchError(err => {
					console.log(err)
					return throwError(err);
				}),
				tap(response => {
					setAuthToken(response.token, true);
					this.loadUser().subscribe();
				})
			);
	}

	logout() {
		clearAuthToken();
		this.loadUser().subscribe();
	}

	loadUser() {
		const token = getAuthToken();
		if (token) {
			return this._getUserByToken();
		} else {
			this.currentUser.next(null);
			return of(null);
		}
	}

	_getUserByToken(): Observable<User> {
		const httpHeaders = getAuthHeaders();
		return this.http.get<User>('api/auth/login', { headers: httpHeaders })
			.pipe(
				catchError(err => {
					console.log(err)
					return throwError(err);
				}),
				tap(value => {
					this.currentUser.next(value);
				})
			);
	}
}