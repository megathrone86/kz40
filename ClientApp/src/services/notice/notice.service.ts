import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Notice } from './notice.interface';
import { scrollToElementIdDelayed } from 'src/helpers/scroll-helper';

@Injectable({
	providedIn: 'root'
})
export class NoticeService {
	onNoticeChanged$: BehaviorSubject<Notice>;

	constructor(
	) {
		this.onNoticeChanged$ = new BehaviorSubject(null);
	}

	hideNotice() {
		this.onNoticeChanged$.next(null);
	}

	setNotice(message: string, type?: undefined | 'success' | 'danger' | 'warning') {
		const notice: Notice = {
			message,
			type
		};
		this.onNoticeChanged$.next(notice);

		scrollToElementIdDelayed("alertNotice");
	}
}
