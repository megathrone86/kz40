export interface Notice {
	type?: string;
	message: string;
}
