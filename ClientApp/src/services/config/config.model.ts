export type ConfigParamName = 'ReCaptchaSiteKey';

export interface GetParamResponse {
    value: string;
}