import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigParamName, GetParamResponse } from './config.model';

@Injectable()
export class ConfigService {
	constructor(
		private http: HttpClient,
	) {
	}

	getParam(name: ConfigParamName) {
		return this.http.get<GetParamResponse>(`api/config/${name}`)
			.pipe(catchError(err => {
				console.log(err)
				return throwError(err);
			}));
	}
}
