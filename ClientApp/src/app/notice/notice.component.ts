import { ChangeDetectorRef, Component, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { NoticeService } from 'src/services/notice/notice.service';
import { Notice } from 'src/services/notice/notice.interface';

@Component({
	selector: 'simple-notice',
	styleUrls: ['./notice.component.scss'],
	templateUrl: './notice.component.html',
})
export class NoticeComponent implements OnInit, OnDestroy {
	@Output() type: any;
	@Output() message: any = '';

	private _subscriptions: Subscription[] = [];

	constructor(
		public noticeService: NoticeService,
		private cdr: ChangeDetectorRef
	) {
	}

	ngOnInit() {
		this._subscriptions.push(this.noticeService.onNoticeChanged$.subscribe(
			(notice: Notice) => {
				notice = Object.assign({}, { message: '', type: '' }, notice);
				this.message = notice.message;
				this.type = notice.type;
				this.cdr.markForCheck();
			}
		));
	}

	ngOnDestroy(): void {
		this._subscriptions.forEach(sb => sb.unsubscribe());
	}

	close() {
		this.noticeService.hideNotice();
	}
}
