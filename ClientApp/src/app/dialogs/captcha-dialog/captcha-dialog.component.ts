// Angular
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfigService } from 'src/services/config/config.service';

@Component({
	selector: 'kt-captcha-dialog',
	templateUrl: './captcha-dialog.component.html',
	styleUrls: ['./captcha-dialog.component.scss'],
})
export class CaptchaDialogComponent implements OnInit {
	public siteKey: string;

	constructor(
		public dialogRef: MatDialogRef<CaptchaDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private configService: ConfigService,
	) {
		configService.getParam('ReCaptchaSiteKey').subscribe(
			response => { this.siteKey = response.value; },
			error => { this.dialogRef.close(null); }
		);
	}

	ngOnInit() {
	}

	resolved(event: string) {
		this.dialogRef.close(event);
	}
}
