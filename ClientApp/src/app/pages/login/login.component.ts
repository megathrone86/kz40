import { Component, ChangeDetectorRef, NgZone } from '@angular/core';
import { AuthService } from 'src/services/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormHelper } from 'src/helpers/form-helper';
import { NoticeService } from 'src/services/notice/notice.service';
import { Subscription } from 'rxjs';
import { User } from 'src/services/auth/auth.model';
import { CaptchaDialogComponent } from 'src/app/dialogs/captcha-dialog/captcha-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.scss'],
})
export class LoginComponent {
  form: FormGroup;
  hasSaveAttempt: boolean = false;
  user: User;
  isReady = false;

  private _subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private auth: AuthService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private ngZone: NgZone,
    private activateRoute: ActivatedRoute,
    private noticeService: NoticeService,
    private dialog: MatDialog,
  ) {
    this.auth.loadUser().subscribe(value => {
      this.isReady = true;
    });

    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.minLength(3),
        Validators.maxLength(320)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ])]
    });
  }

  ngOnInit() {
    this._subscriptions.push(this.auth.currentUser.subscribe(
      value => { this.user = value; }
    ));
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(sb => sb.unsubscribe());
  }

  login() {
    let thisVM = this;
    this.hasSaveAttempt = false;

    if (this.form.invalid) {
      FormHelper.markAllAsTouched(this.form);
    } else {
      const dialogRef = thisVM.dialog.open(CaptchaDialogComponent, { data: {}, width: 'auto' });
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          thisVM.ngZone.run(() => {
            const controls = thisVM.form.controls;
            const email = controls.email.value;
            const password = controls.password.value;
            const recaptchaResponse = res;

            thisVM.auth.login(email, password, recaptchaResponse).subscribe(
              response => {
                thisVM.noticeService.setNotice('Авторизация выполнена', 'success');
                thisVM.cdr.detectChanges();
              },
              error => {
                thisVM.noticeService.setNotice('Ошибка входа', 'danger');
                thisVM.cdr.detectChanges();
              }
            );
          });
        } else {
          thisVM.cdr.detectChanges();
        }
      });
    }
  }

  logout() {
    this.auth.logout();
  }

  changePassword() {
    debugger;
  }

  get f() { return this.form && this.form.controls; }

  hasError(control: any, validationType: string = null) {
    if (validationType)
      return FormHelper.controlHasError(control, validationType);
    else
      return control.invalid && (control.dirty || control.touched || this.hasSaveAttempt);
  }
}
