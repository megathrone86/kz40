import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { RecaptchaModule } from 'ng-recaptcha';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './pages/home/home.component';
import { CounterComponent } from './pages/counter/counter.component';
import { FetchDataComponent } from './pages/fetch-data/fetch-data.component';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from 'src/services/auth/auth.service';
import { LoginComponent } from './pages/login/login.component';
import { NoticeComponent } from './notice/notice.component';
import { ConfigService } from 'src/services/config/config.service';
import { CaptchaDialogComponent } from './dialogs/captcha-dialog/captcha-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  entryComponents: [
    CaptchaDialogComponent,
  ],
  declarations: [
    AppComponent,
    NavMenuComponent,
    FooterComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    LoginComponent,
    NoticeComponent,
    CaptchaDialogComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'login', component: LoginComponent },
    ]),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    OverlayModule,
    RecaptchaModule,
  ],
  providers: [
    AuthService,
    ConfigService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
