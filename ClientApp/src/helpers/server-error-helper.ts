export class ServerErrorHelper {
    public static getError(error: any): string {
        let errorModel = error.error;
        switch (errorModel && errorModel.code) {
            case 'DuplicateTest': return 'Тест с указанными данными уже существует.';
            case 'PasswordMatch': return 'Введенные пароли не совпадают.';
            case 'WrongNewPassword': return 'Введен неверный новый пароль.';
            case 'WrongOldPassword': return 'Введен неверный старый пароль.';
            case 'ValidationError': return errorModel.message;
        }

        return null;
    }
}
