import { HttpHeaders } from '@angular/common/http';

const authTokenKey = 'auth.tkn';

export function getAuthHeaders() {
    const token = getAuthToken();
    if (!token) {
        throw "no token";
    }
    return new HttpHeaders({ 'Authorization': `Bearer ${token}` });
}

export function setAuthToken(token: string, remember: boolean) {
    if (remember) {
        localStorage.setItem(authTokenKey, token);
        sessionStorage.setItem(authTokenKey, '');
        sessionStorage.removeItem(authTokenKey);
    } else {
        sessionStorage.setItem(authTokenKey, token);
        localStorage.setItem(authTokenKey, '');
        localStorage.removeItem(authTokenKey);
    }
}

export function clearAuthToken() {
    sessionStorage.setItem(authTokenKey, '');
    sessionStorage.removeItem(authTokenKey);
    localStorage.setItem(authTokenKey, '');
    localStorage.removeItem(authTokenKey);
}

export function getAuthToken() {
    let userToken = localStorage.getItem(authTokenKey) || sessionStorage.getItem(authTokenKey);
    return userToken;
}