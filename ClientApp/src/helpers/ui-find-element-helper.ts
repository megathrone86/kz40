//Функция в течение указанного интервала пробует найти на странице указанный элемент и выполнить заданные действия
//Как только элемент найден, выполнение функции прерывается.
export function processElementWhenAppears(findElementFunction: () => any, processElementFunction: (HtmlElement) => void, timeoutMS: number = 10000) {
    let timer: NodeJS.Timer;
    timer = setInterval(() => {
        let element = findElementFunction();
        if (element) {
            processElementFunction(element);
            clearInterval(timer);
        }
    }, 1);
    setTimeout(() => clearInterval(timer), timeoutMS);
}