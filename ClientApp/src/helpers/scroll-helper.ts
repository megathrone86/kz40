//Функция в течение указанного интервала пробует найти на странице указанный элемент и проскроллить страницу к нему.

import { processElementWhenAppears } from './ui-find-element-helper';

//Как только элемент найден, выполнение функции прерывается.
export function scrollToElementIdDelayed(elementId: string, timeoutMS: number = 10000) {
    processElementWhenAppears(() => document.getElementById(elementId), (element) => scroll(element), timeoutMS);
}

//Функция в течение указанного интервала пробует найти на странице указанный элемент и проскроллить страницу к нему.
//Как только элемент найден, выполнение функции прерывается.
export function scrollToElementClassDelayed(elementClass: string, useOffset: boolean = true, timeoutMS: number = 10000) {
    processElementWhenAppears(() => {
        let elements = document.getElementsByClassName(elementClass);
        return elements.length ? elements[0] : null;
    }, (element) => scroll(element, useOffset), timeoutMS);
}

export function scrollToTop() {
    window.scroll({ top: 0, behavior: 'smooth' });
}

export function getElementHeight(elementId: string) {
    let $element: any = <HTMLInputElement>document.getElementById(elementId);
    return $element.scrollHeight;
}

function scroll($element: any, useOffset: boolean = true) {
    let offset = 0;

    if (useOffset) {
        //нужно вычислить высоту всех sticky заголовков, чтобы учесть ее при скроллинге
        let elements = document.getElementsByClassName('fixed-scroll-header');
        for (let i = 0; i < elements.length; i++) {
            offset += elements[i].scrollHeight;
        }
    }

    window.scroll({ top: $element.offsetTop - offset, behavior: 'smooth' });
}