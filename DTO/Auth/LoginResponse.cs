﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kz40.DTO.Auth
{
    public class LoginResponse
    {
        public string Token { get; internal set; }
        public string Error { get; internal set; }
    }
}
