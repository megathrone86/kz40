﻿namespace kz40.DTO.Auth
{
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string CaptchaResponse { get; set; }
    }
}
