﻿namespace kz40.DTO.Config
{
    public class GetParamResponse
    {
        public string Value { get; set; }
    }
}
