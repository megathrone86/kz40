﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kz40
{
    public class Consts
    {
        public const string Token_SchemeName = "JwtBearer";
        public const string Token_Issuer = "TestingNet.WebApp";
        public const string Token_Audience = "TestingNet.WebApp";
        public const int Token_LifeTimeMinutes = 60 * 12;
        public const int Token_ClockSkewSeconds = 10;
        public static string TokenSignKeyName = "TokenSignKey.bin";
        public static string TokenEncryptKeyName = "TokenSignKey.bin";

        public const string Claim_UserId = "u";
        public const string Claim_AccessToken = "at";
        public const string Claim_RoleId = "r";

        public const string DefaultAdminName = "admin";
        public const string DefaultAdminEmail = "admin@admin.adm";
        public const string DefaultAdminPassword = "12345678";
    }
}
